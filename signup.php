	<?php
		include('functions/header.php');
		include("functions/sessionchecker.php");
		include("functions/adminsessionchecker.php");
	?>
	<br>
	<div class='col-md-3 col-xs-1'></div>
	<div class="container col-md-6 col-xs-10">
			<?php
				if(isset($_GET['signup'])){
					if($_GET['signup'] == 'success'){
						echo '<div class="alert alert-success alert-dismissible" role="alert"><strong>Success!</strong> User signed up!</div>';
					} else if($_GET['signup'] == 'fail'){
						echo '<div class="alert alert-danger alert-dismissible" role="alert"><strong>Fail!</strong>User could not be signed up! Please try again later or use a different username or email.</div>';
					}		
				}
			?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>
					Signup
				<h4>
			</div>
			<br>
			<table class="table">
				<tr>
					<td>
						<form action="functions/signupsend.php" method="post">
							<br>
							<span class="input-group-addon" id="basic-addon1">Name</span>
							<input type="text" name="name" class="form-control" placeholder="Name" aria-describedby="basic-addon1" required><br>

							<br>
							<span class="input-group-addon" id="basic-addon1">Email</span>
							<input type="text" name="email" class="form-control" placeholder="Email" aria-describedby="basic-addon1" required><br>

							<br>
							<span class="input-group-addon" id="basic-addon1">Username</span>
							<input type="text" name="username" class="form-control" placeholder="Username" aria-describedby="basic-addon1" required><br>

							<br>
							<span class="input-group-addon" id="basic-addon1">Password</span>
							<input type="password" name="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1" required><br>

							<br>
							<span class="input-group-addon" id="basic-addon1">Rank</span>
							<select name="rank" class="selectpicker col-md-12" required>
								<option value="0">Admin</option>
								<option value="1">User</option>
							</select>

							<br>
							<br>
							<center>
								<input class="btn btn-default" type="submit" name="Submit">
							</center>
						</form>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class='col-md-3 col-xs-1'></div>

	<?php
		include('functions/footer.php');
	?>