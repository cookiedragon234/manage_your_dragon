	<?php
		include('functions/header.php');
		include('functions/sessionchecker.php');
	?>
	<br>
	<div class='col-md-3 col-xs-1'></div>
	<div class="container col-md-6 col-xs-10">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>User List</h4>
			</div>
			<table class="table">
			  	<tr>
			  		<td>
			  			<strong>Name</strong>
			  		</td>
			  		<td>
			  			<strong>Username</strong>
			  		</td>
			  	</tr>
				<?php
					$userquery = "SELECT * FROM members WHERE active = '1'";
					$userqueryresult = mysqli_query($connection, $userquery);
					while($userobject = $userqueryresult->fetch_assoc()){
						echo '<tr>';
						echo '<td>' . $userobject['name'] . '</td>';
						echo '<td>' . $userobject['username'] . '</td>';
						echo '</tr>';
					}
				?>
			</table>
		</div>
	</div>
	<div class='col-md-3 col-xs-1'></div>
	<?php
		include('functions/footer.php');
	?>