<?php
include('functions/header.php');
?>
	<br>
	<div class='col-md-3 col-xs-1'></div>
	<div class="container col-md-6 col-xs-10">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>
					<strong>Send Feedback</strong>
				<h4>
			</div>
			<br>
			<table class="table">
				<?php
					if(isset($_POST['feedback'])){
						$feedback = mysql_escape_string($_POST['feedback']);
						$idquery = "SELECT ID from feedback ORDER BY ID DESC LIMIT 1";
						$id2 = mysqli_query($connection, $idquery);
						$iditem = $id2->fetch_assoc();
						$id = $iditem['ID'] + 1;

						$feedback_insert  = "INSERT INTO feedback (";
						$feedback_insert .= "  id, feedback ";
						$feedback_insert .= ") VALUES (";
						$feedback_insert .= " '{$id}', '{$feedback}'";
						$feedback_insert .= ")";
						$feedbackres = mysqli_query($connection, $feedback_insert);
						if($feedbackres){
							echo '<div class="alert alert-success" role="alert"><strong>Success!</strong> Feedback submitted</div>';
						}
					}
				?>
				<tr>
					<td>
						<form action='feedback.php' method='post'>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Feedback</span>
								<textarea type="text" name="feedback" class="form-control" maxlength="1000" placeholder='Is there something wrong? Please send some feedback to our admins...' rows='5' cols='25' name='message' aria-describedby="basic-addon1" required></textarea>
							</div>
							<br>
							<center>
								<button type='submit' class="btn btn-default">Send feedback</button>
							</center>
						</form>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class='col-md-3 col-xs-1'></div>

	<br>
	
	<?php
		include('functions/footer.php');
	?>