function activetab(url, link){
	return url === link;
}

$(document).ready(function(){
	if(window.location.href.search('index.php') > 0 || window.location.href.search('login.php') > 0  ){
		$('.login-tab').addClass('active');
	}
	else if(window.location.href.search('home.php') > 0){
		$('.home-tab').addClass('active');
	} else if(window.location.href.search('messages.php') > 0){
		$('.messages-tab').addClass('active');
	} else if(window.location.href.search('logout.php') > 0){
		$('.logout-tab').addClass('active');
	} else if(window.location.href.search('signup.php') > 0){
		$('.signup-tab').addClass('active');
	} else if(window.location.href.search('userlist.php') > 0){
		$('.userlist-tab').addClass('active');
	} else if(window.location.href.search('feedback.php') > 0){
		$('.feedback-tab').addClass('active');
	} else if(window.location.href.search('settings.php') > 0){
		$('.settings-tab').addClass('active');
	}

	var nocopypaste = document.getElementById('nocopypaste');
	nocopypaste.onpaste = function(e) {
		e.preventDefault();
	}
});