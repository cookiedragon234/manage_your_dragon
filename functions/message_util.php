<?php
	function checkrecipient($recipient, $connection){
		$recipient_query  = "SELECT * ";
		$recipient_query .= "FROM members ";
		$recipient_query .= "WHERE username = '{$recipient}'";
		$recipient_result = mysqli_query($connection, $recipient_query);
		if (isset($recipient_result)){
			return true;
		} else {
			return false;
		}
	}
	function sendmessage($sender, $recipient, $message, $connection){
		$user_exists = checkrecipient($recipient, $connection);
		if($user_exists){
			$message_insert  = "INSERT INTO messages (";
			$message_insert .= "  sender, recipient, message";
			$message_insert .= ") VALUES (";
			$message_insert .= "  '{$sender}', '{$recipient}', '{$message}'";
			$message_insert .= ")";
			$msgsentres = mysqli_query($connection, $message_insert);
			if($msgsentres){
				return true;
			} else{
				return false;
			}
		} else{
			header('location: messages.php?userexists='. $user_exists .'&username=' . $recipient);
		}
	}
	function readmessage($id, $connection){
		$query  = "UPDATE messages SET ";
		$query .= "read_status = '1'";
		$query .= "WHERE ID = '{$id}'";
		mysqli_query($connection, $query);
	}
	function messagelist($user, $connection){
		$message_query  = "SELECT * ";
		$message_query .= "FROM messages ";
		$message_query .= "WHERE recipient = '{$user}' ";
		$message_query .= "ORDER BY ID DESC";
		$result = mysqli_query($connection, $message_query);
		return $result;
	}
?>