<?php

include('encryption.php');
include('connection.php');

$username = mysql_escape_string($_POST["username"]);
$password = mysql_escape_string($_POST["password"]);

// 2. Perform database query
$query  = "SELECT * ";
$query .= "FROM members ";
$query .= "WHERE username='{$username}'";
$query .= "LIMIT 1";
$result = mysqli_query($connection, $query);

// Test if there was a query error
if (!$result) {
	header('location: ../login.php?wrongcred=true');
} else {
	//$result = mysqli_fetch_assoc($result);

	//check password!
	$credential = mysqli_fetch_assoc($result);

	if (verify_password($password, $credential['password'])){
		if($credential['active'] == 1){
			session_start();
			$_SESSION['username'] = $credential['username'];
			$_SESSION['rank'] = $credential['rank'];
			header('Location:../home.php');
		}
	} else {
	  	// wrong password!
	  	header('location: ../login.php?wrongcred=true');
	}
}
//echo 'Something went wrong... (⌐■_■)--︻╦╤─ - - - (╥﹏╥) (dead)';
?>