<?php
	include('../functions/connection.php');
	include('../functions/encryption.php');

	$name = ucfirst(mysql_escape_string($_POST["name"]));
	$username = mysql_escape_string($_POST["username"]);
	$email = mysql_escape_string($_POST["email"]);
	$password = mysql_escape_string($_POST["password"]);
	$rank = mysql_escape_string($_POST["rank"]);
	$idquery = "SELECT ID from members ORDER BY ID DESC LIMIT 1";
	$id2 = mysqli_query($connection, $idquery);
	$iditem = $id2->fetch_assoc();
	$id = $iditem['ID'] + 1;
	$saltedpassword = generate_hash($password);

	$query_insert  = "INSERT INTO members (";
	$query_insert .= "  ID, name, username, email, password, rank";
	$query_insert .= ") VALUES (";
	$query_insert .= "  '{$id}', '{$name}', '{$username}', '{$email}', '{$saltedpassword}', '{$rank}'";
	$query_insert .= ")";
	
	$signupresult = mysqli_query($connection, $query_insert);

	if($signupresult){
		header('location: ../signup.php?signup=success');
	} else{
		header('location: ../signup.php?signup=fail');
	};
?>