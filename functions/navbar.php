
<?php

//start of the navbar
    echo <<<_END
    <nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">MYD</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
_END;

if(isset($_SESSION['username'])){
	echo '<li role="presentation" class="home-tab">';
	echo '<a href="home.php"><span class="glyphicon glyphicon-home"></span> Home</a>';
	echo '</li>';
	echo '<li role="presentation" class="messages-tab">';
	echo '<a href="messages.php"><span class="glyphicon glyphicon-envelope"></span> Messages <span class="badge">' . $num_messages . '</span></a>';
	echo '</li>';
	echo '<li role="presentation" class="userlist-tab">';
	echo '<a href="userlist.php"><span class="glyphicon glyphicon-user"></span> User List</a>';
	echo '</li>';
	echo '<li role="presentation" class="feedback-tab">';
	echo '<a href="feedback.php"><span class="glyphicon glyphicon-comment"></span> Send Feedback</a>';
	echo '</li>';
	echo '<li role="presentation" class="settings-tab">';
	echo "<a href='settings.php'><span class='glyphicon glyphicon-cog'></span> <strong><i>" . ucfirst($_SESSION['username']) . "</i></strong>'s settings</a>";
	echo '</li>';
	if($_SESSION['rank'] == '0'){
		echo '<li role="presentation" class="signup-tab">';
		echo '<a href="signup.php"><span class="glyphicon glyphicon-edit"></span> Signup</a>';
		echo '</li>';
	}
	echo '<li role="presentation" class="logout-tab">';
	echo '<a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a>';
	echo '</li>';
} else{
	echo '<li role="presentation" class="login-tab">';
	echo '<a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a>';
	echo '</li>';
}

    echo <<<_END
	      </ul>
		</div>
		</div>
	</nav>
_END;



	

?>