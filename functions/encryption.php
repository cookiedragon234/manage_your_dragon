<?php

function generate_hash($password){
	if(defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
		$salt = '$2y$11$LVH6wxr7s2ZHpmHS9SJUzE';
		return crypt($password, $salt);
	}
}


function verify_password($password, $saltedpassword){
	return substr(crypt($password, $saltedpassword),0,50) == substr($saltedpassword,0,50);
}


?>