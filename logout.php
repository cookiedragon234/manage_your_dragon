<?php
include('functions/header.php');
unset($_SESSION);
session_unset();
session_destroy();
$timetill = 10;
header('refresh: 10; url=index.php'); // redirect the user after 10 seconds
#exit; // note that exit is not required, HTML can be displayed.
?>
<center>
	<h1>You have been logged out...</h1>
	<h3>If you believe this is an error please contact the <a href="mailto:cookiedragon234@gmail.com">System Administrator</a> and he will get back to you as soon as possible.</h3>
	<h3>You will be redirected to the home page (where you can login) in <a id="counter">10</a> seconds...</h3>
	<h3>Or get <a href="index.php">redirected now</a></h3>
	<script type="text/javascript">
		function countdown() {
		    var i = document.getElementById('counter');
		    if (parseInt(i.innerHTML)<=0) {
		        location.href = 'index.php';
		    }
		    i.innerHTML = parseInt(i.innerHTML)-1;
		}
		setInterval(function(){ countdown(); },1000);
	</script>
</center>
<?php
	include('functions/footer.php');
?>