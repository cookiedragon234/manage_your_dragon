	<?php
		include('functions/header.php');
		include('functions/encryption.php');
	?>
	<br>
	<div class='col-md-3 col-xs-1'></div>
	<div class="container col-md-6 col-xs-10">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>
					<strong><i><?php echo ucfirst($_SESSION['username']); ?></i></strong>'s settings
				<h4>
			</div>
			<br>
			<table class="table">
				<?php
					if(isset($_POST['oldpassword']) && isset($_POST['newpassword']) && isset($_POST['newpasswordrepeat'])){
						$newpassword = mysql_escape_string($_POST['newpassword']);
						$newpasswordrepeat = mysql_escape_string($_POST['newpasswordrepeat']);
						if($newpassword == $newpasswordrepeat){
							$oldpassword = mysql_escape_string($_POST['oldpassword']);
							$oldpassquery = "SELECT * FROM members WHERE username = '{$_SESSION['username']}'";
							$oldpwdresult = mysqli_query($connection, $oldpassquery);
							if($oldpwdresult){
								$data = $oldpwdresult->fetch_assoc();
								$correctpassword = verify_password($oldpassword, $data['password']);
								if($correctpassword){
									$saltednewpassword = generate_hash($newpassword);
									$updatepwd = "UPDATE members SET password = '{$saltednewpassword}' WHERE username = '{$_SESSION['username']}'";
									$updateresult = mysqli_query($connection, $updatepwd);
									if($updateresult){
										echo '<div class="alert alert-success" role="alert"><strong>Success!</strong> Password updated.</div>';
									}
								} else{
									echo '<div class="alert alert-danger" role="alert">Wrong password</div>';
								}
							} else {
								echo '<div class="alert alert-danger" role="alert">user not found</div>';
							}
						} else{
							echo '<div class="alert alert-danger" role="alert">New passwords did not match</div>';
						}
					} else {
					}
				?>
				<tr>
					<td>
						<p><b>Change your password:</b></p>
						<form action="settings.php" method="post">
							<span class="input-group-addon" id="basic-addon1">Old Password</span>
							<input type="password" name="oldpassword" class="form-control" placeholder="Password" aria-describedby="basic-addon1" required>
							<br>
							<span class="input-group-addon" id="basic-addon1">New Password</span>
							<input type="password" name="newpassword" class="form-control" placeholder="Password" aria-describedby="basic-addon1" required>
							<br>
							<span class="input-group-addon" id="basic-addon1">Repeat New Password</span>
							<input type="password" name="newpasswordrepeat" class="form-control" placeholder="Password" aria-describedby="basic-addon1" required>
							<br><br>
							<center>
								<button type='submit' class="btn btn-default">Change Password</button>
							</center>
						</form>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class='col-md-3 col-xs-1'></div>

	<?php
		include('functions/footer.php');
	?>