<?php
	include('functions/header.php');
	include ('functions/connection.php');
	include('functions/message_util.php');
	$current_user = $_SESSION['username'];
	include("functions/sessionchecker.php");
?>

<div>
	<br>
	<?php
		if(isset($_GET['userexists']) && isset($_GET['username'])){
			echo '<div class="alert alert-danger" role="alert">The user <strong>"' . $_GET['username'] . '"</strong> could not be found</div>';
		}
	?>
	<br>
	<div class='col-md-2'></div>
	<div class="container col-md-8 col-xs-12">

		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading"><h4><strong><i><?php echo ucfirst($current_user); ?></i></strong>'s messages</h4></div>

			<!-- Table -->
			<table class="table">
			<?php
				// table header
				echo '<tr><td><strong>Sender</strong></td><td><strong>Message</strong></td><td><strong>Read status</strong></td></tr>';
				$num_messages = 0;
				$message_query_result = messagelist($current_user, $connection);
				if (!$message_query_result){
					echo 'error!';
					echo $message_query_result;
				} else{
					while ($item = $message_query_result->fetch_assoc()){
						$num_messages = $num_messages + 1;
						if($_SESSION['username'] == $item['sender']){
							echo '<tr><td>You</td>';
						} else{
							echo '<tr><td>' . $item['sender'] . '</td>';
						}
						if($item['read_status'] == '0' || $item['read_status'] == 0 ){
							echo '<td><b>' . $item ['message'] . '</b> </td>';
							echo '<td><a href="messages.php?setread=' . $item['ID'] . '">Set message as "read"</a></td></tr>';
						} else{
							echo '<td>' . $item ['message'] . '</td><td>Already Read</td></tr>';
						}
					}
					if($num_messages < 1){
						echo '<div class="alert alert-warning" role="alert"><strong>You have no messages</strong> If this is an error, please try refreshing the page</div>';
					}
				}
			?>
			</table>
		</div>
	</div>
	<div class='col-md-2'></div>

	<br>
	<div class='col-md-3'></div>
	<div class="container col-md-6 col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4><strong>Send message</strong><h4>
			</div>
			<table class="table">
				<tr>
					<td>
						<form action='messages.php' method='post'>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Recipient<button type="button" class=" userlistmodal" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-question-sign"></span></button></span>
								<input type="text" class="form-control" name="recipient" placeholder="Username" aria-describedby="basic-addon1" required>
							</div>
							<br>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Message</span>
								<textarea type="text" name="message" class="form-control" placeholder='message' rows='5' cols='25' name='message' aria-describedby="basic-addon1" required></textarea>
							</div>
							<br>
							<center>
								<button type='submit' class="btn btn-default">Send Message</button>
							</center>
						</form>
					</td>
				</tr>
			</table>
			<?php
			if(isset($_POST['recipient'])){
				$sender = $current_user;
				$recipient = mysql_escape_string($_POST['recipient']);
				$message = mysql_escape_string($_POST['message']);
				$msgsentres2 = sendmessage($sender, $recipient, $message, $connection);
				if($msgsentres2){
					echo '<div class="alert alert-success" role="alert">Message sent</div>';
				} else{
					echo '<div class="alert alert-fail" role="alert">Message could not be sent</div>';
				}
			}
			if(isset($_GET['setread'])){
				$id = $_GET['setread'];
				readmessage($id, $connection);
			}

			?>
		</div>
	</div>
	<div class='col-md-3'></div>

	<br>


</div>
<br>




<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">User List</h4>
      </div>
      <div class="modal-body">
	        <table class="table">
		  	<tr>
		  		<td>
		  			<strong>Name</strong>
		  		</td>
		  		<td>
		  			<strong>Username</strong>
		  		</td>
		  	</tr>
			<?php
				$userquery = "SELECT * FROM members WHERE active = '1'";
				$userqueryresult = mysqli_query($connection, $userquery);
				while($userobject = $userqueryresult->fetch_assoc()){
					echo '<tr>';
					echo '<td>' . $userobject['name'] . '</td>';
					echo '<td>' . $userobject['username'] . '</td>';
					echo '</tr>';
				}
			?>
		</table>
      </div>
    </div>
  </div>
</div>

<?php
include('functions/footer.php');
?>